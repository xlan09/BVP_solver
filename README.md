- Trajectories are parametrized by spline and BVP solver is used to find the parameters to connect two states, which consists of (x, y, theta, linear_velocity, angular_velocity).

- The following is the planned trajectories with initial heading 0 and final heandings ranging from -pi to pi.

![Gui is not shown properly, click the png file to view directly.](v_00.60-v_f0.60-trajectories.png)
