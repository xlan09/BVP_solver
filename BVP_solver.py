#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from world import World
import logging


class VelocityProfile(object):
    """
    Linear velocity profile
    """
    def __init__(self, v_0, v_f, max_acceleration=0.3):
        if v_0 < 0 or v_f < 0 or max_acceleration <= 0:
            raise ValueError('Invalid parameters to construct velocity profile')

        self.v_0 = v_0
        self.v_f = v_f
        self.max_acceleration = max_acceleration

    def velocity(self, time):
        """
        Get velocity
        """

        if self.v_f > self.v_0:
            acceleration = self.max_acceleration
        elif self.v_f < self.v_0:
            acceleration = -1.0 * self.max_acceleration
        else:
            acceleration = 0

        if acceleration == 0:
            velocity = self.v_f
        else:
            time_to_reach_final_velocity = (self.v_f - self.v_0) / float(acceleration)

            if time <= time_to_reach_final_velocity:
                velocity = self.v_0 + acceleration * time
            else:
                velocity = self.v_f

        return velocity


class AngularProfile(object):
    """
    Angular velocity profile
    """
    def __init__(self, control_points, t):
        self.control_points = control_points
        self.time_span = t
        self.spline = Spline(control_points, len(control_points) - 1, t)

    def angular_velocity(self, time):
        return self.spline.value(time)


class BVPSolver(object):
    """
    BVP solver
    """

    @classmethod
    def solve(cls, world, start_state, goal_state, timeStep, initial_parameters, parameter_variance, tolerance, max_iterations=50):
        """
        """

        if Util.isCloseToZero(timeStep) or timeStep < 0 or Util.isCloseToZero(parameter_variance) or \
                        parameter_variance < 0 or Util.isCloseToZero(tolerance) or tolerance < 0:
            raise ValueError('Time step, variance and tolerance must be positive')

        logger = logging.getLogger(__name__)
        logger.debug('###################################################################')
        logger.debug('start state {}, goal state {}, time step {}, parameter variance {}, tolerance {}'.format(
            start_state, goal_state, timeStep, parameter_variance, tolerance))

        # angular velocity profile first and last control point are fixed since the state is [x, y, theta, v, w] in lattice
        parameters = np.array(initial_parameters, dtype=float)
        errors = []

        count = 1
        while 1:
            logger.debug('***********Iteration {}***************'.format(count))
            logger.debug('curr parameters {}'.format(parameters))

            velocity_profile, angular_profile = cls._convert_parameters_to_profiles(start_state, goal_state, parameters)
            simulated_final_state = cls._forward_simulation(world, start_state, velocity_profile,
                                                                   angular_profile, parameters[-1], timeStep)
            final_state_error = simulated_final_state - goal_state
            errors.append(final_state_error.vector_form())

            logger.debug('final state error {}'.format(final_state_error))

            if final_state_error.close_to_zero(tolerance):
                logger.debug('stop because error is within tolerance {}, count={}'.format(tolerance, count))
                break
            elif count >= max_iterations:
                logger.debug('stop because maximum iterations {} reached'.format(max_iterations))
                break

            jacobian = []
            for j in range(len(parameters)):
                jacobian.append(cls._calculate_partial_derirative_j(world, start_state, goal_state, parameters, parameter_variance, timeStep, j).vector_form())

            jacobian = np.column_stack(jacobian)
            logger.debug('jacobian: {}'.format(jacobian))

            parameters += (-1) * np.dot(np.linalg.pinv(jacobian), final_state_error.vector_form())
            logger.debug('updated parameters are {}'.format(parameters))

            count += 1

        # convert final parameters to profile
        velocity_profile, angular_profile = cls._convert_parameters_to_profiles(start_state, goal_state, parameters)
        return velocity_profile, angular_profile, parameters, np.array(errors)

    @classmethod
    def _calculate_partial_derirative_j(cls, world, start_state, goal_state, curr_parameters, parameter_variance, timeStep, j):
        """
        Calculate partial derirative of the state error with respect to the j-th parameter
        """

        small_variance_vector = []
        for k in range(len(curr_parameters)):
            if k != j:
                small_variance_vector.append(0)
            else:
                small_variance_vector.append(parameter_variance)

        small_variance_vector = np.array(small_variance_vector)

        parameters = curr_parameters + small_variance_vector
        velocity_profile, angular_profile = cls._convert_parameters_to_profiles(start_state, goal_state, parameters)
        right_error = cls._forward_simulation(world, start_state, velocity_profile, angular_profile, parameters[-1], timeStep) - goal_state

        parameters = curr_parameters - small_variance_vector
        velocity_profile, angular_profile = cls._convert_parameters_to_profiles(start_state, goal_state, parameters)
        left_error = cls._forward_simulation(world, start_state, velocity_profile, angular_profile, parameters[-1], timeStep) - goal_state

        return (right_error - left_error) / (2.0 * parameter_variance)

    @staticmethod
    def _forward_simulation(world, start_state, velocity_profile, angular_profile, final_time, time_step):
        """
        Forward simulation
        """

        curr_pose = start_state.pose

        # append time t in the end
        times = np.append(np.arange(0, final_time, time_step), final_time)
        for index, time in enumerate(times):
            if index < len(times) - 1:
                dt = times[index + 1] - times[index]
                curr_pose = world.vehicle.dynamics(curr_pose, velocity_profile.velocity(time),
                                                   angular_profile.angular_velocity(time), dt)

        # now curr_pose is the simulated pose at time t_f
        # since we used clamped spline, the spline will cross the last control point
        return State(curr_pose, velocity_profile.velocity(final_time), angular_profile.angular_velocity(final_time))

    @staticmethod
    def _convert_parameters_to_profiles(start_state, goal_state, parameters):
        """
        Construct velocity profile and angular profile based on current parameters
        """

        velocity_profile = VelocityProfile(start_state.linear_velocity, goal_state.linear_velocity)
        control_points = np.append(np.insert(parameters[:-1], 0, start_state.angular_velocity),
                                   goal_state.angular_velocity)
        angular_profile = AngularProfile(control_points, parameters[-1])

        return velocity_profile, angular_profile


class State(object):
    """
    Lattice planner state
    """
    def __init__(self, pose, linear_velocity, angular_velocity):
        self.pose = np.array(pose)
        self.linear_velocity = linear_velocity
        self.angular_velocity = angular_velocity

    def __add__(self, other):
        return State(self.pose + other.pose, self.linear_velocity + other.linear_velocity,
                     self.angular_velocity + other.angular_velocity)

    def __sub__(self, other):
        return State(self.pose - other.pose, self.linear_velocity - other.linear_velocity, self.angular_velocity - other.angular_velocity)

    def __mul__(self, other):
        if isinstance(other, float):
            return State(self.pose * other, self.linear_velocity * other, self.angular_velocity * other)
        else:
            raise TypeError('State multiplication only supports multiplication by float')

    def __div__(self, other):
        if isinstance(other, float):
            if np.abs(other) < 1e-12:
                raise ValueError('State can not be divided by zero')

            return State(self.pose / other, self.linear_velocity / other, self.angular_velocity / other)
        else:
            raise TypeError('State division only supports division by float')

    def vector_form(self):
        return np.append(self.pose, [self.linear_velocity, self.angular_velocity])

    def close_to_zero(self, tolerance):
        res = True
        vec = self.vector_form()
        for i in range(len(vec)):
            if np.abs(vec[i]) > tolerance:
                res = False
                break

        return res

    def __str__(self):
        return str(self.vector_form())


class Spline(object):
    """
    Spline
    """

    def __init__(self, control_points, degree, time_span=1):
        self.control_points = self._initialize_control_points(control_points)
        self.degree = degree
        # internal knots
        internal_knots = np.linspace(0, time_span, len(self.control_points) - self.degree + 1)
        # use clamped spline, so make the first d knots and last d knots equal
        # this way we will make sure the spline cross the last control point
        self.knots = np.append(np.ones(self.degree) * internal_knots[0], internal_knots)
        self.knots = np.append(self.knots, np.ones(self.degree) * internal_knots[-1])

    def value(self, t):
        res = 0
        for i, point in enumerate(self.control_points):
            res += self._b_i_d(i, self.degree, t) * point

        return res

    def _b_i_0(self, i, t):
        """
        Calculate the basis function B_{i,0}(t) = 1 if t_i <= t < t_{i+1} else 0
        :param i:
        :param t:
        :return:
        """

        if i > len(self.control_points) + self.degree - 1:
            raise ValueError('knots vector index out of range')

        # if two time knots are equal, return 0
        if self.knots[i] == self.knots[i+1]:
            return 0
        else:
            # here we make t <= self.knots[i+1] instead of <, to make sure the last point on the spline curve coincide
            # with the last control point. If we use < here, the last point will be 0 on the spline curve if t=1
            return 1 if self.knots[i] <= t <= self.knots[i+1] else 0

    def _b_i_d(self, i, d, t):
        """
        Calculate basis function for higher degrees B_{i,d}(t), here recursion is used, which is not good if d is large
        More efficient implementation should use matrix representation of B-splines
        :param i:
        :param d: degree
        :param t:
        :return:
        """

        if d == 0:
            return self._b_i_0(i, t)
        else:
            # deal with two time knots are equal
            tolerance = 1e-12
            if abs(self.knots[i + d] - self.knots[i]) < tolerance and abs(self.knots[i + d + 1] - self.knots[i + 1]) < tolerance:
                return 0
            elif abs(self.knots[i + d] - self.knots[i]) < tolerance:
                return (self.knots[i + 1 + d] - t) / (self.knots[i + d + 1] - self.knots[i + 1]) * self._b_i_d(i + 1, d-1, t)
            elif abs(self.knots[i + d + 1] - self.knots[i + 1]) < tolerance:
                return (t - self.knots[i]) / (self.knots[i + d] - self.knots[i]) * self._b_i_d(i, d-1, t)
            else:
                return (t - self.knots[i]) / (self.knots[i + d] - self.knots[i]) * self._b_i_d(i, d-1, t) + \
                       (self.knots[i + 1 + d] - t) / (self.knots[i + d + 1] - self.knots[i + 1]) * self._b_i_d(i + 1, d-1, t)

    def _initialize_control_points(self, control_points):
        res = []
        for point in control_points:
            res.append(np.array(point))

        return res


class Util(object):
    """
    Util class
    """

    @staticmethod
    def isCloseToZero(val):
        tolerance = 1e-12

        return np.abs(val) < tolerance

    @staticmethod
    def setup_logging(filename, logging_level):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging_level)

        handler = logging.FileHandler(filename, mode='w')
        handler.setLevel(logging_level)

        # create a logging format
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)

        logger.addHandler(handler)

        return logger