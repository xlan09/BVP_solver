#!/usr/bin/env python

import unittest
from world import World
from BVP_solver import Util, BVPSolver, State
import logging
import numpy as np
import matplotlib.pyplot as plt
import math


class LatticPlannerTest(unittest.TestCase):
    """
    Unit test
    """

    def setUp(self):
        self.world = World()
        # test BVP solver
        Util.setup_logging('LogTestBVPSolver.txt', logging.DEBUG)

    def test_BVPsolver(self):
        trajectory_fig_handle = 1
        plt.figure(trajectory_fig_handle)
        plt.title('Trajectories')

        dt, parameter_variance, tolerance = 0.1, 0.01, 0.015
        max_linear_velocity = 0.6
        v_0 = max_linear_velocity
        v_f = max_linear_velocity
        start_heading = 0
        dist = 2

        start_pose = [0, 0, start_heading]
        self.world.vehicle.pose = start_pose
        start_state = State(start_pose, v_0, 0)

        heading_grid_sets = [ np.arange(start_pose[-1], np.pi, np.pi / 20)]
        heading_grid_sets.append((-1.0) * heading_grid_sets[0])

        for heading_grid in heading_grid_sets:
            # First we solve a BVP which is along straight line, so the control pints for angular velocities is close to 0
            initial_parameters = [0, 0.01, 0.02, 0.03, dist / max_linear_velocity]
            for heading in heading_grid:
                goal_state = State([dist, 0, heading], v_f, 0)

                # use 5th degree spline to parametrize the angular velocity, so we have first 4 parameters for the middle 4 control
                # points for the spline, the last parameter is travel time to reach the goal_state
                velocity_profile, angular_profile, optimal_parameters, errors = BVPSolver.solve(self.world, start_state, goal_state, dt,
                                                                            initial_parameters,
                                                                            parameter_variance, tolerance)
                initial_parameters = optimal_parameters

                # plot
                self.plot_motion_primitives(start_state, self.world.vehicle.dynamics, velocity_profile, angular_profile, dt,
                                            trajectory_fig_handle)
                # self.plot_solver_info(start_state, goal_state, velocity_profile, angular_profile, errors, dt)

        trajectory_filename = 'v_0{:.2f}-v_f{:.2f}-trajectories.png'.format(start_state.linear_velocity,
                                                                              goal_state.linear_velocity)
        plt.figure(trajectory_fig_handle)
        plt.savefig(trajectory_filename)

    def plot_motion_primitives(self, start_state, vehicle_dynamics, velocity_profile, angular_profile, dt, trajectory_fig_handle):
        """
        Plot solver information
        """

        # plot velocity and angular profile
        travel_time = angular_profile.time_span
        times = np.append(np.arange(0, travel_time, dt), travel_time)

        # plot footprint
        curr_pose = start_state.pose
        trace = [curr_pose[:2]]
        for index, time in enumerate(times):
            if index < len(times) - 1:
                time_step = times[index + 1] - times[index]
                curr_pose = vehicle_dynamics(curr_pose, velocity_profile.velocity(time),
                                             angular_profile.angular_velocity(time), time_step)
            trace.append(curr_pose[:2])

        trace = np.array(trace)
        plt.figure(trajectory_fig_handle)
        plt.plot(trace[:, 0], trace[:, 1], 'ro-')

    def plot_solver_info(self, start_state, goal_state, velocity_profile, angular_profile, errors, dt):
        filename_prefix = 'v_0{:.2f}-v_f{:.2f}-theta_0{:.2f}-theta_f{:.2f}'.format(start_state.linear_velocity,
                                                                                   goal_state.linear_velocity,
                                                                                   start_state.pose[-1],
                                                                                   goal_state.pose[-1])

        # plot errors
        fig_errors = plt.figure()
        plt.title('Solver errors')
        for j in range(errors.shape[1]):
            plt.plot(range(errors.shape[0]), errors[:, j], 'o-', label=str(j + 1) + '-th state')
        plt.legend(loc=4)
        fig_errors.savefig(filename_prefix + '-errors.png')

        # plot velocity and angular profile
        travel_time = angular_profile.time_span
        times = np.append(np.arange(0, travel_time, dt), travel_time)

        linear_velocities = [velocity_profile.velocity(time) for time in times]
        angular_velocities = [angular_profile.angular_velocity(time) for time in times]

        plt.figure()
        plt.title('Linear velocities')
        plt.plot(times, linear_velocities)
        plt.savefig(filename_prefix + '-linear_velocities.png')

        plt.figure()
        plt.title('Angular velocities')
        plt.plot(times, angular_velocities)
        plt.savefig(filename_prefix + '-angular_velocities.png')

    def _convert_to_legal_key(self, dist, v_0, v_f, heading_diff):
        return (self._round_to(dist), self._round_to(v_0), self._round_to(v_f), self._round_to(heading_diff))

    def _round_to(self, val):
        return int(val * 10000) / 10000.0

    def _is_equal(self, list1, list2):
        res = True
        for val1, val2 in zip(list1, list2):
            if math.fabs(val1 - val2) > 1e-6:
                res = False
                break

        return res


if __name__ == '__main__':
    unittest.main()




